﻿using Lab05.BUS;
using Lab05.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab05.GUI
{
    public partial class frmQuanLySinhVien : Form
    {

        private readonly StudentService studentService = new StudentService();
        private readonly FacultyService facultyService = new FacultyService();
        string  imageLocation = null;

        public frmQuanLySinhVien()
        {
            InitializeComponent();
            dgvStudent.CellClick += new DataGridViewCellEventHandler(this.dgvStudent_CellClick);
        }
        public void setGridViewStyle(DataGridView dgview)
        {
            dgview.BorderStyle = BorderStyle.None;
            dgview.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dgview.CellBorderStyle =
            DataGridViewCellBorderStyle.SingleHorizontal;
            dgview.BackgroundColor = Color.White;
            dgview.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void frmQuanLySinhVien_Load(object sender, EventArgs e)
        {
            try
            {
                setGridViewStyle(dgvStudent);
                var listFacultys = facultyService.GetAll();
                var listStudents = studentService.GetAll();
                FillFalcultyCombobox(listFacultys);
                BindGrid(listStudents);
                imgAvatar.Image = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //Hàm binding list dữ liệu khoa vào combobox có tên hiện thị là tên khoa, giá trị là Mã khoa
        private void FillFalcultyCombobox(List<Faculty> listFacultys)
        {
            listFacultys.Insert(0, new Faculty());
            this.cmbKhoa.DataSource = listFacultys;
            this.cmbKhoa.DisplayMember = "FacultyName";
            this.cmbKhoa.ValueMember = "FacultyID";
        }

        //Hàm binding gridView từ list sinh viên
        private void BindGrid(List<Student> listStudent)
        {
            dgvStudent.Rows.Clear();
            foreach (var item in listStudent)
            {
                int index = dgvStudent.Rows.Add();
                dgvStudent.Rows[index].Cells[0].Value = item.StudentID;
                dgvStudent.Rows[index].Cells[1].Value = item.FullName;
                if (item.Faculty != null)
                    dgvStudent.Rows[index].Cells[2].Value =
                    item.Faculty.FacultyName;
                dgvStudent.Rows[index].Cells[3].Value = item.AverageScore + "";
                if (item.Major != null)
                    dgvStudent.Rows[index].Cells[4].Value = item.Major.Name + "";
                ShowAvatar(item.Avatar);
            }
        }
        private void ShowAvatar(string ImageName)
        {
            if (string.IsNullOrEmpty(ImageName))
            {
                imgAvatar.Image = null;
            }
            else
            {
                string parentDirectory =
                Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName;
                string imagePath = Path.Combine(parentDirectory, "Images", ImageName);
                imgAvatar.Image = Image.FromFile(imagePath);
                imgAvatar.Refresh();
            }
        }
        

        private void cbDangKyChuyenNganh_CheckedChanged(object sender, EventArgs e)
        {
            var listStudents = new List<Student>();
            if (this.cbDangKyChuyenNganh.Checked)
                listStudents = studentService.GetAllHasNoMajor();
            else
                listStudents = studentService.GetAll();
            BindGrid(listStudents);
        }

        private void btnChonAnh_Click(object sender, EventArgs e)
        {
            
            try
            {
                OpenFileDialog fileOpen = new OpenFileDialog();
                fileOpen.Title = "Chọn hình ảnh sinh viên";
                fileOpen.Filter = "Hình ảnh(*.jpg; *.jpeg; *.png)| *.jpg; *.jpeg; *.png | All files(*.*) | *.* ";
                if (fileOpen.ShowDialog() == DialogResult.OK)
                {
                    imageLocation = fileOpen.FileName;
                    imgAvatar.Image = Image.FromFile(imageLocation);
                    
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi không thể upload ảnh! ", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAdd_Update_Click(object sender, EventArgs e)
        {

            Model1 context = new Model1();
            Student s = context.Student.FirstOrDefault(p => p.StudentID == txtMSSV.Text);
            if (s != null)
            {
                s.StudentID = txtMSSV.Text;
                s.FullName = txtTenSV.Text;
                s.AverageScore = float.Parse(txtDiemTB.Text);
                s.FacultyID = (cmbKhoa.SelectedItem as Faculty).FacultyID;
                s.Avatar = imageLocation;
                context.SaveChanges();
                MessageBox.Show("Cập nhật thành công!", "Thông báo", MessageBoxButtons.OK);
                BindGrid(context.Student.ToList());
            }
            else
            {
                float.TryParse(txtDiemTB.Text, out float value);
                if (txtMSSV.Text == "" || txtTenSV.Text == "" || txtDiemTB.Text == "")
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin!", "Thông báo", MessageBoxButtons.OK);
                }
                else if (txtMSSV.TextLength != 10)
                {
                    MessageBox.Show("Mã số sinh viên phải có 10 ký tự!", "Thông báo", MessageBoxButtons.OK);
                }
                else if (value > 10)
                {
                    MessageBox.Show("Điểm từ 1 đến 10!", "Thông báo", MessageBoxButtons.OK);
                }
                else
                {

                    Student stuAdd = new Student();
                    stuAdd.StudentID = txtMSSV.Text;
                    stuAdd.FullName = txtTenSV.Text;
                    stuAdd.AverageScore = float.Parse(txtDiemTB.Text);
                    stuAdd.FacultyID = (cmbKhoa.SelectedItem as Faculty).FacultyID;
                    stuAdd.Avatar = imageLocation;
                    context.Student.Add(stuAdd);
                    context.SaveChanges();
                    MessageBox.Show("Thêm thành công!", "Thông báo", MessageBoxButtons.OK);
                    txtMSSV.Text = "";
                    txtTenSV.Text = "";
                    txtDiemTB.Text = "";
                    //imgAvatar.Image = null;
                    BindGrid(context.Student.ToList());
                }
            }        
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            btnAdd_Update.Enabled = false;
            if ( txtMSSV.Text.Trim() != "" || txtTenSV.Text.Trim() != "" || txtDiemTB.Text.Trim() != "" )
            {
                Student stuUpdate = studentService.findStudent(txtMSSV.Text);
                if (stuUpdate != null)
                {
                    if (MessageBox.Show($"Bạn muốn xóa sinh viên {txtTenSV.Text}?",  "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        studentService.DeleteStudent(txtMSSV.Text);
                        //LoadData();
                        Model1 context = new Model1();
                        BindGrid(context.Student.ToList());
                       
                        //Refresh();
                        txtMSSV.Text = "";
                        txtTenSV.Text = "";
                        txtDiemTB.Text = "";
                        MessageBox.Show("Xóa dữ liệu thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtMSSV.Enabled = true;
                        btnAdd_Update.Enabled = true;
                    }
                }
                else
                {
                    MessageBox.Show("Không tìm thấy sinh viên trong hệ thống!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void txtMSSV_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void dgvStudent_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dgvStudent.CurrentRow.Index;
            txtMSSV.Text = dgvStudent.Rows[index].Cells[0].Value.ToString();
            txtTenSV.Text = dgvStudent.Rows[index].Cells[1].Value.ToString();
            cmbKhoa.Text = dgvStudent.Rows[index].Cells[2].Value.ToString();
            txtDiemTB.Text = dgvStudent.Rows[index].Cells[3].Value.ToString();

            Model1 context = new Model1();
            Student st = context.Student.FirstOrDefault(p => p.StudentID == txtMSSV.Text);
            string path = st.Avatar;
            ShowAvatar(path);
        }
        private void TbxDKCN_Click(object sender, EventArgs e)
        {
            frmDangKyChuyenNganh tbxDkcn = new frmDangKyChuyenNganh();
            tbxDkcn.ShowDialog();
        }

        private void btnRefesh_Click(object sender, EventArgs e)
        {
            Model1 context = new Model1();
            BindGrid(context.Student.ToList());
        }

        private void BoxExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
