﻿using Lab05.BUS;
using Lab05.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab05.GUI
{
    public partial class frmDangKyChuyenNganh : Form
    {
        public frmDangKyChuyenNganh()
        {
            InitializeComponent();
        }

        private readonly StudentService studentService = new StudentService();
        private readonly FacultyService facultyService = new FacultyService();
        private readonly MajorService majorService = new MajorService();

        private void frmDangKyChuyenNganh_Load(object sender, EventArgs e)
        {
            try
            {
                var listFacultys = facultyService.GetAll();
                FillFalcultyCombobox(listFacultys);
                Faculty selectedFaculty = cmbKhoa.SelectedItem as Faculty;
                var listStudents = studentService.GetAllhasNoMajor(selectedFaculty.FacultyID);
                BindGrid(listStudents);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //Hàm binding list dữ liệu khoa vào combobox có tên hiện thị là tên khoa, giá trị là Mã khoa
        private void FillFalcultyCombobox(List<Faculty> listFacultys)
        {
            this.cmbKhoa.DataSource = listFacultys;
            this.cmbKhoa.DisplayMember = "FacultyName";
            this.cmbKhoa.ValueMember = "FacultyID";
        }
        private void FillMajorCombobox(List<Major> listMajor)
        {
            this.cmbChuyenNganh.DataSource = listMajor;
            this.cmbChuyenNganh.DisplayMember = "Name";
            this.cmbChuyenNganh.ValueMember = "MajorID";
        }
        private void cmbKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            Faculty selectedFaculty = cmbKhoa.SelectedItem as Faculty;
            if (selectedFaculty != null)
            {
                var listMajor = majorService.getAllByFaculty(selectedFaculty.FacultyID);
                FillMajorCombobox(listMajor);
                var listStudents = studentService.GetAllhasNoMajor(selectedFaculty.FacultyID);
                BindGrid(listStudents);
            }
        }

        private void BindGrid(List<Student> listSstudent)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in listSstudent)
            {
                int index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells[1].Value = item.StudentID;
                dataGridView1.Rows[index].Cells[2].Value = item.FullName;
                if (item.Faculty != null)
                    dataGridView1.Rows[index].Cells[3].Value = item.Faculty.FacultyName;
                dataGridView1.Rows[index].Cells[4].Value = item.AverageScore + "";
                if (item.MajorID != null)
                    dataGridView1.Rows[index].Cells[5].Value = item.Major.Name + "";
            }

        }

        private void tbxRegister_Click(object sender, EventArgs e)
        {
            Model1 context = new Model1(); // Tạo một instance của DbContext
            Major selectedMajor = cmbChuyenNganh.SelectedItem as Major; // Lấy chuyên ngành được chọn từ combobox

            if (selectedMajor != null)
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    bool isSelected = Convert.ToBoolean(row.Cells[0].Value);
                    if (isSelected)
                    {
                        string studentID = row.Cells[1].Value.ToString();

                        // Tìm sinh viên trong cơ sở dữ liệu
                        var student = context.Student.SingleOrDefault(x => x.StudentID == studentID);
                        if (student != null)
                        {
                            // Cập nhật chuyên ngành cho sinh viên
                            student.MajorID = selectedMajor.MajorID; // Gán MajorID từ selectedMajor vào student
                            studentService.Insertupdate(student);
                        }
                    }
                }
                context.SaveChanges(); // Lưu thay đổi vào cơ sở dữ liệu

                // Cập nhật DataGridView
                Faculty selectedFaculty = cmbKhoa.SelectedItem as Faculty;
                if (selectedFaculty != null)
                {
                    var listStudents = studentService.GetAllhasNoMajor(selectedFaculty.FacultyID);
                    BindGrid(listStudents);
                }
            }
        }
    }
}
