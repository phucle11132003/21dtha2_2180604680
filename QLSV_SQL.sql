USE [master]
GO
/****** Object:  Database [QLSV]    Script Date: 20/10/2023 8:46:11 SA ******/
CREATE DATABASE [QLSV]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QLSV', FILENAME = N'D:\SQL Server 2022 Enterprise_2\MSSQL16.SQL2022\MSSQL\DATA\QLSV.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'QLSV_log', FILENAME = N'D:\SQL Server 2022 Enterprise_2\MSSQL16.SQL2022\MSSQL\DATA\QLSV_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [QLSV] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLSV].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLSV] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLSV] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLSV] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLSV] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLSV] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLSV] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QLSV] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLSV] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLSV] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLSV] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLSV] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLSV] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLSV] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLSV] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLSV] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLSV] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLSV] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLSV] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLSV] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLSV] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLSV] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLSV] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLSV] SET RECOVERY FULL 
GO
ALTER DATABASE [QLSV] SET  MULTI_USER 
GO
ALTER DATABASE [QLSV] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLSV] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLSV] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLSV] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [QLSV] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [QLSV] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'QLSV', N'ON'
GO
ALTER DATABASE [QLSV] SET QUERY_STORE = ON
GO
ALTER DATABASE [QLSV] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [QLSV]
GO
/****** Object:  Table [dbo].[Faculty]    Script Date: 20/10/2023 8:46:11 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Faculty](
	[FacultyID] [int] NOT NULL,
	[FacultyName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Faculty] PRIMARY KEY CLUSTERED 
(
	[FacultyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Major]    Script Date: 20/10/2023 8:46:11 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Major](
	[FacultyID] [int] NOT NULL,
	[MajorID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Major] PRIMARY KEY CLUSTERED 
(
	[FacultyID] ASC,
	[MajorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student]    Script Date: 20/10/2023 8:46:11 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[StudentID] [nvarchar](10) NOT NULL,
	[FullName] [nvarchar](255) NOT NULL,
	[AverageScore] [float] NOT NULL,
	[FacultyID] [int] NULL,
	[MajorID] [int] NULL,
	[Avatar] [nvarchar](255) NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Faculty] ([FacultyID], [FacultyName]) VALUES (1, N'Công Nghệ Thông Tin')
INSERT [dbo].[Faculty] ([FacultyID], [FacultyName]) VALUES (2, N'Ngôn Ngữ Anh')
INSERT [dbo].[Faculty] ([FacultyID], [FacultyName]) VALUES (3, N'Quản Trị Kinh Doanh')
GO
INSERT [dbo].[Major] ([FacultyID], [MajorID], [Name]) VALUES (1, 1, N'Công Nghệ Phần Mềm')
INSERT [dbo].[Major] ([FacultyID], [MajorID], [Name]) VALUES (1, 2, N'Hệ Thống Thông Tin')
INSERT [dbo].[Major] ([FacultyID], [MajorID], [Name]) VALUES (1, 3, N'An Toàn Thông Tin')
INSERT [dbo].[Major] ([FacultyID], [MajorID], [Name]) VALUES (2, 1, N'Tiếng Anh Thương Mại')
INSERT [dbo].[Major] ([FacultyID], [MajorID], [Name]) VALUES (2, 2, N'Tiếng Anh Truyền Thông')
GO
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180600015', N'Nguyễn Thái An', 6.183732082, 2, 2, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180600375', N'Nguyễn Thị Thu Hằng ', 2.30163077, 2, 1, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180600432', N'Phan Hồng Hòa', 0.5990694358, 1, 3, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180600509', N'Nguyễn Đỗ Quốc Huy', 4.725343058, 1, 3, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180600637', N'Đỗ Vương Khánh', 7.175720802, 2, 2, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180600793', N'Huỳnh Thanh Long', 1.518227248, 2, 2, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180600933', N'Trần Phương Nam ', 5.213441904, 2, 2, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180601016', N'Phan Nguyễn', 2.62089893, 1, 3, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180601234', N'k', 8, 2, NULL, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180601408', N'Nguyễn Thảo Trường Thanh ', 6.118251791, 2, 1, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180601738', N'Phạm Thanh Tú', 0.6734961738, 1, 1, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180601761', N'Nguyễn Khánh Tuấn', 0.8218423237, 1, 3, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180601792', N'Đoàn Thị Kim Tuyền', 1.006746181, 1, 3, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180602677', N'Phùng Bá Nguyên', 7.520718339, 2, 1, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180602996', N'Dương Phú Thịnh', 9.631458819, 1, 3, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180603186', N'Nguyễn Minh Tú', 1.696502478, 1, 3, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180603333', N'Nguyễn Chí Cường', 9.713134193, 1, 1, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180603383', N'Bùi Đức Hiếu', 1.071627075, 1, 1, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180603867', N'Danh Huynh Thanh Nhat', 8.776929993, 2, 2, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180604143', N'Dương Quốc Thắng', 2.737268974, 2, 1, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180604426', N'Đoàn Quốc Thắng', 3.589672268, 2, 1, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180604436', N'Nguyễn Duy Thuận', 1.288015857, 1, 3, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180604563', N'Phạm Anh Hào', 2.007515234, 1, 3, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180604813', N'Bùi Phi Dương', 1.530058353, 1, 3, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180604857', N'Trần Hữu Hoàng', 0.4672393963, 2, 2, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180604923', N'Trương Lê Minh Nhật', 8.933180957, 1, 1, NULL)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID], [MajorID], [Avatar]) VALUES (N'2180605768', N'Nguyễn Hoàng Duy', 9.944210576, 2, 2, NULL)
GO
ALTER TABLE [dbo].[Major]  WITH CHECK ADD  CONSTRAINT [FK_Major_Faculty] FOREIGN KEY([FacultyID])
REFERENCES [dbo].[Faculty] ([FacultyID])
GO
ALTER TABLE [dbo].[Major] CHECK CONSTRAINT [FK_Major_Faculty]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK_Student_Faculty] FOREIGN KEY([FacultyID])
REFERENCES [dbo].[Faculty] ([FacultyID])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK_Student_Faculty]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK_Student_Major] FOREIGN KEY([FacultyID], [MajorID])
REFERENCES [dbo].[Major] ([FacultyID], [MajorID])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK_Student_Major]
GO
USE [master]
GO
ALTER DATABASE [QLSV] SET  READ_WRITE 
GO
